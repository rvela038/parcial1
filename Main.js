new Vue({
    el: '#app',
    data: function() {
      return {
        nombreUsuario:'',
        apellidoUsuario:'',
        ciudadUsuario:'',
        direccionUsuario:'',
        sexoUsuario:'',
        estCivUsuario:'',
        estUsuario:'',
        nombreProveedor:'',
        ingresoProveedor:'',
        capitalProveedor:'',
        ubicacionProveedor:'',
        entregaProveedor:'',
        tipoEmp:'',
        productosProveedor:'',
        usuarioNombre:'',
        contraUsuario:'',
        numTar:'',
        nomPro:'',
        codSeg:'',
        fechaExp:'',
        ciudad:'',
        ciudadespty: [{
            value: 'Bocas del Toro',
            label:'Bocas del Toro'},
            {value:'Chiriquí',
            label:'Chiriquí '},
            {value:'Coclé ',
            label:'Coclé '},
            {value:'Colón ',
            label:'Colón '},
            {value:'Darién',
            label:'Darién '},
            {value:'Herrera',
            label:'Herrera '},
            {value:'Panama Oeste',
            label:'Panama Oeste'},
            {value:'Panamá',
            label:'Panamá'}],
        productos:'',
        listaProductos: [{
            value:'Computadoras',
            label:'Computadoras'},
            {value:'Celulares',
            label:'Celulares'},
            {value:'Televisores',
            label:'Televisores'},
            {value:'Audífonos',
            label:'Audífonos'},
            {value:'Equipos de sonido',
            label:'Equipos de sonido'},
            {value:'Tablets',
            label:'Tablets'},
        ],
        tableData1: [{
            nombre:'Ricardo',
            apellido:'Velasco',
            ciudad:'Panamá',
            sexo:'Masculino',
            estadoCivil:'Soltero/a',
            estado: 'Activo',
            direccion:'Panama Oeste'
        }],
        tableData2: [{
            nombreProv:'Ricardo',
            fechaIn:Date(),
            capital:'Panamá',
            ubicacionProv:'Masculino',
            tiempoEntrega:Date(),
            tipoEmp: 'Privada',
            productoProv:'Celulares'
        }],      
      }
 },
    methods:{
        validar: function(url){
            if (this.usuarioNombre === 'admin' && this.contraUsuario === 'admin'){
                window.location = "file:///C:/Users/Ricardo%20Velasco/Desktop/Parcial%201%20Prog.%20Grafica/Form_Usuario.html";
                
            }
            else {
                alert('inaccesible')
                document.getElementById("nombre").value="";
                document.getElementById("clave").value="";
                document.getElementById("nombre").focus();
            }
        }
  },
    guardarUser: function(){
            this.tableData1.push({
                nombre: this.nombreUsuario,
                apellido: this.apellidoUsuario,
                ciudad: this.ciudadUsuario,
                sexo: this.sexoUsuario,
                estadoCivil: this.estCivUsuario,
                estado: this.estUsuario,
                direccion: this.direccionUsuario
              })
            this.nombreUsuario='',
              this.apellidoUsuario='',
              this.ciudadUsuario='',
              this.sexoUsuario='',
              this.estCivUsuario='',
              this.estUsuario='',
              this.direccionUsuario=''
              console.log (this.nombre, this.apellido, this.ciudad, this.sexo, this.estadoCivil, this.estado, this.direccion)
 },
    guardarProv: function(){
            this.tableData2.push({
                nombreProv: this.nombreProveedor,
                fechaIn: this.ingresoProveedor,
                capital: this.capitalProveedor,
                ubicacionProv: this.ubicacionProveedor,
                tiempoEntrega: this.entregaProveedor,
                tipoEmp: this.tipoEmp,
                productoProv: this.productosProveedor
              })
            this.nombreProveedor='',
              this.ingresoProveedor='',
              this.capitalProveedor='',
              this.ubicacionProveedor='',
              this.entregaProveedor='',
              this.tipoEmp='',
              this.productosProveedor=''
              console.log (this.nombreProv, this.fechaIn, this.capital, this.ubicacionProv, this.tiempoEntrega, this.tipoEmp, this.productosProveedor)
    },  
    computed: {
        validarUser: function () {
            return this.nombreUsuario && this.apellidoUsuario && this.ciudadUsuario && this.sexoUsuario && this.estCivUsuario && this.estUsuario && this.direccionUsuario
        },
        validarProv: function () {
            return this.nombreProveedor && this.ingresoProveedor && this.capitalProveedor > 0 && this.ubicacionProveedor && this.entregaProveedor && this.tipoEmp && this.productosProveedor
        },
        
        
        validar: function (){
            return this.id && this.nombre && this.cantidad > 0 && this.precio > 0.0 && this.fecha && this.descripcion
            },
            buscar_todo: function () {
              const index = this.tableData.findIndex(item => item.nombre === this.buscar);
              console.log(index)
              if (index > -1) {
                console.log(this.tableData[index])
                return true
              }
              return false
            }

    }
  })
